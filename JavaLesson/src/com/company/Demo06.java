package com.company;

/**
 * 强制类型转换：比如下面将double类型的变量转换为int类型的
 * 注意：转换的时候并没有四舍五入，而是直接截断的，有可能会造成数据的丢失
 */

public class Demo06 {
    public static void main(String[] args) {
        double heightAvg1 = 75.8;
        int heightAvg2 = (int) heightAvg1;
        System.out.println(heightAvg1);
        System.out.println(heightAvg2);
    }
}
