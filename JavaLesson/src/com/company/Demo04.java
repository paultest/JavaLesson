package com.company;

/**
 * Java有两类数据类型，基本数据类型和引用数据类型
 * 基本数据类型：整数类型（byte、short、int、long）、浮点类型（float、double）、字符型（char）、布尔型（boolean）
 * 引用数据类型：类（class）、接口（interface）、数组
 *
 * 常用基本数据类型：
 * int 整型 4字节  用于存储整数如年龄、个数
 * double 双精度浮点型 8字节 用于存储小数，如商品价格、平均分
 * float  单精度浮点型 4字节 用于存储小数，如身高
 * char 字符型 2字节  用于存储单个字符，如性别
 * boolean 布尔型 1字节  表示真或假  取值只能true或者是false
 *
 * float类型，为变量赋值时要在数值后添加字母f,如 float height = 175.2f
 * char类型，为变量赋值时要使用单引号，如char level = 'A';
 *
 * double 和 float 的区别：
 * double精度高，有效数字16位，float精度7位
 * double消耗内存是float的两倍，double的运算速度比float慢得多
 * 能用单精度（float）时不要用双精度（以省内存，加快运算速度）
 *
 * char 和 String 的区别：
 * char表示字符，定义时用单引号，只能存储一个字符，如char c='x';
 * String表示字符串，定义时用双引号，可以存储一个或多个字符，如String name="tom";
 * char是基本数据类型，而String 是一个类，具有面向对象的特征，可以调用方法，如name.length()获取字符串的长度。
 */

public class Demo04 {
    public static void main(String[] args) {
        String name = "爱慕课";
        char sex = '男';
        int num = 18;
        double price = 120.5;
        boolean isOK = true;
        System.out.println(name);
        System.out.println(sex);
        System.out.println(num);
        System.out.println(price);
        System.out.println(isOK);
    }
}
