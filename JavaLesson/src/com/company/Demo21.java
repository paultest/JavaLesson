package com.company;

import java.util.Scanner;

/**
 * 接收用户输入信息：
 * 1. 导入Scanner类  import java.util.Scanner;
 * 2. 创建Scanner对象 Scanner input = new Scanner(System.in);
 * 3. 接收输入信息 int score = input.nextInt();
 *
 * 注意：
 * input.next();//获取String类型数据
 * input.nextDouble();//double类型
 * input.nextInt();//int类型next();
 */

public class Demo21 {
    public static void main(String [] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入考试分数");
        int score = input.nextInt();

        if (score > 60) {
            System.out.println("及格");
        } else {
            System.out.println("不及格");
        }
    }
}
