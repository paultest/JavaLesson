package com.company;

/**
 * 逻辑运算符主要用于进行逻辑运算。Java 中常用的逻辑运算符有：
 * 与：&&
 * 或：||
 * 非：!
 * 异或：^
 */

public class Demo12 {
    public static void main(String[] args) {
        boolean a = true; // a同意
        boolean b = false; // b反对
        boolean c = false; // c反对
        boolean d = true; // d同意
        System.out.println((a && b) + " 未通过");
        System.out.println((a || b) + " 通过");
        System.out.println((!a) + " 未通过");
        System.out.println((c ^ d) + " 通过");
    }
}
