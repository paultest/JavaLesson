package com.company;

/**
 * 在 Java 中，我们通过三个元素描述变量：变量类型、变量名以及变量值，根据所需要保存的数据的格式，将其保存在指定类型的变量空间中，并且通过变量名快速定位
 * String love = "imooc"; 其中呢 String 是变量类型，love是变量名，后面是变量值
 * 注意：变量值不能使用单引号，否则编译的时候会报错
 *
 * 命名变量：
 * 变量名是由字母、下划线_、美元符号$、数字组成的，其中数字不能开头，即3name这个变量名是不合法的
 * 变量名不允许使用关键字来命名，比如private等变量名
 * 变量名由多单词组成时，第一个单词的首字母小写，其后单词的首字母大写，俗称骆驼式命名法（也称驼峰命名法），如 myAge
 * 变量命名时，尽量简短且能清楚的表达变量的作用，做到见名知意。如：定义变量名 stuName 保存“学生姓名”信息
 * 注意：Java是区分大小写的，即Name和name是两个不同的变量，函数名也是一样
 */

public class Demo03 {
    public static void main(String [] args) {
        String love = "imooc";

        System.out.println("变量love的内容是：" + love);

        love = "I love imooc";

        System.out.println("重新赋值后变量love的内容是：" + love);
    }
}
