package com.company;

/**
 * 比较运算符用于判断两个数据的大小，例如：大于、等于、不等于。比较的结果是一个布尔值（ true 或 false ）。
 */

public class Demo11 {
    public static void main(String[] args) {
        int a = 16;
        double b = 9.5;
        String str1 = "hello";
        String str2 = "imooc";
        System.out.println("a等于b：" + (a == b));
        System.out.println("a大于b：" + (a > b));
        System.out.println("a小于等于b：" + (a <= b));
        System.out.println("str1等于str2：" + (str1 == str2));
    }
}
