package com.company;

/**
 * Java 中注释有三种类型：单行注释、多行注释、文档注释
 *
 * 文档注释：
 * @author 标明开发该类模块的作者
 * @version 标明该类模块的版本
 * @see 参考转向，也就是相关主题
 * @param 对方法中某参数的说明
 * @return 对方法返回值的说明
 * @exception 对方法可能抛出的异常进行说明
 */

/**
 * 这是文档注释
 * @author Administrator
 * @version v1.0
 */
public class Demo08 {
    /**
     * 这是多行注释
     * 可以包括多行内容
     */
    public static void main(String [] args) {
        // 这是单行注释
        System.out.println("imooc");
        // System.out.println("imooc");
    }
}
