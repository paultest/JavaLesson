package com.company;

/**
 * 定义常量： final 常量名 = 值;
 * 注意：常量名一般使用大写字母
 */

public class Demo07 {
    public static void main(String[] args) {
        final String MALE = "男";
        final String FEMALE = "女";
        System.out.println(MALE);
        System.out.println(FEMALE);
    }
}
