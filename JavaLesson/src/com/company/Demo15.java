package com.company;

/**
 * Java 常用的 3 种循环： while 、 do...while 、 for
 *
 * 执行过程：
 * 判断while后面的条件是否成立
 * 当条件成立时，执行循环内的操作代码，然后重复执行1,2，直到循环条件不成立为止
 *
 * 特点：先判断后执行
 */

public class Demo15 {
    public static void main(String[] args) {

        int i = 1; // 代表 1 - 5 之间的数字

        // 当变量小于等于 5 时执行循环
        while (i < 6) {

            // 输出变量的值，并且对变量加 1，以便于进行下次循环条件判断
            System.out.println(i);
            i++;
        }
    }
}
